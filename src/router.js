import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Containers from './views/Containers.vue'
import Servidores from './views/Servidores.vue'
import Usuarios from './views/Usuarios.vue'
import Login from './views/Login.vue'
import HomeU from './viewsUsers/HomeU.vue'
import ContainersU from './viewsUsers/ContainersU.vue'
import ServidoresU from './viewsUsers/ServidoresU.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',

  routes: [

    // loads Home component
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/inicio',
      name: 'inicio',
      component: Home
    },
    {
      path: '/containers',
      name: 'containers',
      component: Containers
    },
    {
      path: '/servidores',
      name: 'servidores',
      component: Servidores
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuarios
    },
    {
      path: '/containers/:sectionSlug',
      name: 'dynamicContainers',
      // la división de código a nivel de ruta genera 
      // un fragmento separado (contenido dinámico. [hash] .js) 
      // para esta ruta que se carga con retraso cuando se visita la ruta.
      component: () => import(/* webpackChunkName: "dynamicContainers" */ './views/DynamicContainers.vue')
    },
    {
      path: '/inicioU',
      name: 'inicioU',
      component: HomeU
    },
    {
      path: '/containersU',
      name: 'containersU',
      component: ContainersU
    },
    {
      path: '/servidoresU',
      name: 'servidoresU',
      component: ServidoresU
    },
    {
      path: '/containersU/:sectionSlug',
      name: 'dynamicContainersU',
      // la división de código a nivel de ruta genera 
      // un fragmento separado (contenido dinámico. [hash] .js) 
      // para esta ruta que se carga con retraso cuando se visita la ruta.
      component: () => import(/* webpackChunkName: "dynamicContainers" */ './viewsUsers/DynamicContainersU.vue')
    },

  ]
})

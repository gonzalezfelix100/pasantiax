import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResource from 'vue-resource'
import VModal from 'vue-js-modal'
import VueSession from 'vue-session'

 

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(VModal,{ dynamic: true, dynamicDefaults: { clickToClose: false } })
Vue.use(VueSession, {persist: true})

window.bus = new Vue();

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
